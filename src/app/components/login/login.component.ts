import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  getFacebookToken() {
    return null;
  }

  loginWithFacebook() {
    const token = this.getFacebookToken();
    this.userService.loginUser(token).subscribe( response => {});
    this.router.navigate(['/messenger']);
  }

}
