import { Component,
         OnInit,
         Input} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.scss']
})
export class MessengerComponent implements OnInit {
    @Input()chatroomId;
    chatRoom = {
        messages: []
    };
    newMessage: FormGroup = new FormGroup({
        text: new FormControl(null, Validators.required)
    });

    constructor(private messageService: MessageService) {}

    ngOnInit() {
        this.messageService.getChatRoomMessages(this.chatroomId).subscribe(response => {
            this.chatRoom.messages = response;
        });
    }

    sendMessage() {
        this.messageService.newMessage(this.newMessage.value).subscribe(response => {
            this.chatRoom.messages.push(response);
            this.newMessage.reset();
        });
    }
}
