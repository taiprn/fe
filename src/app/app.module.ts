import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ChatroomService } from './services/chatroom/chatroom.service';
import { MessageService } from './services/message/message.service';
import { UserService } from './services/user/user.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    ChatroomService,
    MessageService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
