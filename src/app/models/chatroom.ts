import { User } from './user';
import { Message } from './message';

export class ChatRoom {
    id: number;
    users: User[];
    allowedUsers: User[];
    messageHistory: Message[];
}
