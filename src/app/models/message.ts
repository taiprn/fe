import { User } from './user';

export class Message {
    id: number;
    user: User  ;
    content: String;
    timestamp: Date;
}
