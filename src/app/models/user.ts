export class User {
    available: boolean;
    username: string;
    displayName: string;
}
