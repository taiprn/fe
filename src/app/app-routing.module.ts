import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { MessengerComponent } from './components/messenger/messanger.component';

const appRoutes: Routes = [
  {
    path: '/login',
    component: LoginComponent
  }, {
    path: 'messenger',
    component: MessengerComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
