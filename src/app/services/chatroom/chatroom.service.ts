import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { User } from '../../models/user';
import { Message } from '../../models/message';
import { ChatRoom } from '../../models/chatroom';

@Injectable()
export class ChatroomService {
  constructor() { }

  newChatRoom(name: string): Observable<ChatRoom> {
    return undefined;
  }

  getChatRoom(id: number): Observable<ChatRoom> {
    return undefined;
  }

  getChatRoms(username: string): Observable<ChatRoom[]> {
    return undefined;
  }

  getSentMessages(chatRoomId: number, username: string): Observable<Message[]> {
    return undefined;
  }

  getRecievedMessages(chatRoomId: number, username: string): Observable<Message[]> {
    return undefined;
  }

  addUser(username: string, chatRoomId: number): Observable<any> {
    return undefined;
  }

  sendMessage(chatRoomId: number, message: Message): Observable<any> {
    return undefined;
  }

  recieveMessage(chatRoomId: number,
                 user: User,
                 message: Message): Observable<any> {
    return undefined;
  }

  presentChat(chatRoomId: number): Observable<Message[]> {
    return undefined;
  }
}
