import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { User } from '../../models/user';
import { Message } from '../../models/message';
import { ChatRoom } from '../../models/chatroom';

@Injectable()
export class MessageService {
  constructor(private http: HttpClient) { }

  newMessage(content: string): Observable<any> {
    return this.http.post('/message', content);
  }

  getChatRoomMessages(chatRoomId: number): Observable<any> {
    return this.http.get(`chatRoom/${chatRoomId}/message`);
  }

  getUserMessages(username: string): Observable<any> {
    return this.http.get(`user/${username}/message`);
  }
}
