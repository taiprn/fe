import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { User } from '../../models/user';
import { Message } from '../../models/message';
import { ChatRoom } from '../../models/chatroom';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) { }

  loginUser(username: string, password?: string): Observable<any> {
    if (!password) {
      // login with token
      return this.http.post('/login/token', username);
    }
    return this.http.post('/login', {username: username, password: password});
  }

  getUser(username: string): Observable<User> {
    return undefined;
  }

  updateUser(user: User): Observable<User> {
    return undefined;
  }

  createUser(username: string, password: string): Observable<User> {
    return undefined;
  }

  dispatchMessage(message: Message, room: ChatRoom): Observable<any> {
    return undefined;
  }

  addFriend(username: string, friendName: string): Observable<any> {
    return undefined;
  }

  removeFriend(username: string, friendName: string): Observable<any> {
    return undefined;
  }

  joinChatRoom(username: string, chatRoomId: number): Observable<any> {
    return undefined;
  }

  leaveChatRoom(username: string, chatRoomId: number): Observable<any> {
    return undefined;
  }

  inviteToChatRoom(username: string, chatRoomId: number): Observable<any> {
    return undefined;
  }
}
